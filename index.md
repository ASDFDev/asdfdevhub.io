# Setsuna
*Attendance taking using near-ultrasound*

[Learn more](background.md)

## Android
- [Source code](https://github.com/ASDFDev/PAS-Quiet-Android)
- [Prototype](https://github.com/ASDFDev/PAS-Nearby-Android)

## iOS
- [Source code](https://github.com/ASDFDev/PAS-Quiet-iOS)
- [Prototype](https://github.com/ASDFDev/PAS-Nearby-iOS)

## Web
- [Backend (PHP)](https://github.com/ASDFDev/PAS-Backend)
- [Backend (ASP.NET)](https://github.com/ASDFDev/PAS-Backend-ASPNET)


[![Setsuna video](http://img.youtube.com/vi/GuQ-Y_PzM-E/0.jpg)](https://www.youtube.com/watch?v=GuQ-Y_PzM-E "Proximity Attendance System")

## Authors:
- Daniel Quah(emansih)
- Justin Xin(Minatosan)

## Acknowledgement
Special thanks to Mr. Teo Shin Jen for guiding us throughout this project. This project will not be possible without Brian Armstrong's [Quiet library](https://github.com/quiet/).







`This project is not endrosed by Singapore Polytechnic nor does it help you to circumvent the attendance taking system.` 
